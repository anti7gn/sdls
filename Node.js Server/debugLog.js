var mongoose = require('mongoose');
var logSchema = require('./database').logSchema;
var configs = require('./configs');
var moment = require('moment');
var logTableName = configs.db_log_table_name;
var db_directory = configs.database_directory;

mongoose.connect(db_directory);
var db = mongoose.connection;

var debugLog = {}

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
});

var logDatabase = db.model(logTableName, logSchema);

// ==================================================
// time , room id , key id , action , debug
debugLog.log_action = function(r_id, k_id, act, de) {
	var time = moment().valueOf();
	var room_id = r_id;
	var key_id = k_id;
	var action = act;
	var debug = de;

	var logJSON = {
		'time': time,
		'room_id': room_id,
		'key_id': key_id,
		'action': action,
		'debug': debug
	};
	console.log(debug+' - '+action);
	var logObject = new logDatabase(logJSON);
	logObject.save(function(err) {
		if (err) {
			console.log('E: ' + err);
		}
	});
}

// ==================================================
// log function for record log 
// and print log out on server console if enable
debugLog.log = function(message, room_id, key_id, action) {
	var enable = configs.enable_debug_log;
	if (enable) {
		console.log(message);
	}
	debugLog.log_action(room_id, key_id, action, message);
}

// ==================================================
// query log from parameters and return array log result to callback
debugLog.show_log = function(room_id, key_id, action, start, rows, timeS, timeE, callback) {
	var enable = configs.enable_debug_log;
	var limit = rows || 10;
	var skip = start || 10;
	skip -= 10;
	var time_start = timeS;
	var time_end = timeE;
	var json;
	if (room_id != undefined) {
		json = {
			'room_id': room_id,
			'time': {
				$gte: time_start,
				$lt: time_end
			}
		};

	} else if (key_id != undefined) {
		json = {
			'key_id': key_id,
			'time': {
				$gte: time_start,
				$lt: time_end
			}
		};
	} else {
		json = {
				'time': {
					$gte: time_start,
					$lt: time_end
				}
		};
	}
	// print log json object
	if (enable) {
		// console.log(json);
	}
	var finish = false;
	var result_num = 0;
	logDatabase.find(json, function(err, result) {
		result_num = result.length;
		logDatabase.find(json).sort({
			time: -1
		}).skip(skip).execFind(function(err, logs) {
			var array_logs = [];
			if (!err && logs.length > 0) {
				var count = 0;
				logs.forEach(function(log) {
					count++;
					var json = {
						'time': log.time,
						'room_id': log.room_id,
						'key_id': log.key_id,
						'action': log.action,
						'debug': log.debug
					};
					array_logs.push(json);
					if ((count >= logs.length || count >= limit) && !finish) {
						finish = true;
						callback({
							'result': array_logs,
							'num_logs': result_num
						});
					}
				});
			} else {
				console.log('E:  ' + err+' or cannot find any log');
				callback({
					'result': array_logs,
					'num_logs': 0
				});
			}
		});

	});

}

// ==================================================
// return array of logs to callback
debugLog.show_all_log = function(callback) {
	var array_logs = [];
	var count = 0;
	logDatabase.find({}, function(err, logs) {
		if (!err) {
			logs.forEach(function(log) {
				count++;
				var json = {
					'time': log.time,
					'room_id': log.room_id,
					'key_id': log.key_id,
					'action': log.action,
					'debug': log.debug
				};
				array_logs.push(json);
				if (count >= logs.length) {
					console.log(array_logs.length);
					callback(array_logs);
				}
			});
		} else {
			console.log(err);
			callback(array_logs);
		}
	});
}

// ==================================================
// remove all log from database
debugLog.remove_all_log = function() {
	logDatabase.remove({}, function(err) {
		debugLog.log(logTableName + ' removed');
	});
}

module.exports = debugLog;