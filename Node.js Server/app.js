var https = require('https');
var fs = require('fs');
var express = require('express');
var app = express();
var totpGenerator = require('./totpGenerator');
var configs = require('./configs');
var debugLog = require('./debugLog');
var mongoose = require('mongoose');
var database = require('./database');
var router = express.Router();
var moment = require('moment');
var _ = require('underscore');

var options = {
	key: fs.readFileSync('server.key'),
	cert: fs.readFileSync('server.crt')
};

var server = https.createServer(options, app);
server.listen(2407);


var db = mongoose.connection;

// mongoose.connect(configs.database_directory);
db.on('error', function callback() {
	console.error.bind(console, 'connection error:');
});
db.once('open', function callback() {

});

var room_table_name = configs.db_room_table_name;
var key_table_name = configs.db_key_table_name;
var index_table_name = configs.db_index_table_name;;

var roomsDatabase = db.model(room_table_name, database.roomSchema);
var keyDatabase = db.model(key_table_name, database.keySchema);
var indexDatabase = db.model(index_table_name, database.indexSchema);
// log(configs);

log('I: listen on 2407 port');

var middleware = [express.bodyParser()];

app.get('/*', function(req, res, next) {
	res.setHeader("Access-Control-Allow-Origin", "*");
	next(); // http://expressjs.com/guide.html#passing-route control
});

// ==================================================
// GET method 
// SDL debug log panel
app.get(configs.path_debug_log, function(req, res) {

	if (req.query.key_id == undefined && req.query.room_id == undefined && req.query.time_start == undefined && req.query.time_end == undefined) {
		var nowDate = moment().subtract('d', 2).valueOf();
		var end = moment().add('d', 2).valueOf();
		var base_url = configs.base_url_server;
		var log_url_path = configs.path_debug_log;
		res.redirect(base_url + log_url_path + '?time_start=' + nowDate + '&time_end=' + end + '&start=10');
		res.end();
	} else {
		var key_id = req.query.key_id;
		var room_id = req.query.room_id;
		var time_start = req.query.time_start;
		if (time_start == undefined)
			time_start = 0;

		var time_end = req.query.time_end;
		if (time_end == undefined)
			time_end = moment().add('d', 1).valueOf();

		var start = req.query.start;
		var rows = req.query.rows;
		var action = req.query.action;
		var jsonQuery = {
			'key_id': key_id,
			'room_id': room_id,
			'time_start': time_start,
			'time_end': time_end,
			'start': start,
			'rows': rows
		};
		console.log('D: get log');

		res.writeHeader(200, {
			"Content-Type": "text/html"
		});
		var result = debugLog.show_log(room_id, key_id, action, start, rows, time_start, time_end, function(result_json) {
			var count = 0;
			var string = [];
			var result = result_json.result;
			var number_logs = result_json.num_logs;
			var finish = false;

			if (result.length > 0 && number_logs > 0) {
				result.forEach(function(log) {
					count++;
					var json = {
						'time': log.time,
						'room_id': log.room_id,
						'key_id': log.key_id,
						'action': log.action,
						'debug': log.debug
					};
					string.push(JSON.stringify(json));
					if ((count >= result.length || count >= 10) && !finish) {

						finish = true;
						fs.readFile('./template.html', 'utf8', function(err, html) {
							if (err) {
								log('E: ' + err);
								res.write(err);
								res.end();
							} else {
								// replace string with array of JSON logs
								var str_html = html.toString();
								str_html = str_html.replace('[]', string);
								str_html = str_html.replace('[-]', JSON.stringify(number_logs));
								res.write(str_html);
								res.end();
							}

						});
					}
				});
			} else {
				fs.readFile('./template.html', 'utf8', function(err, html) {
					if (err) {
						log('E: ' + err);
						res.write(err);
						res.end();
					} else {
						// replace string with array of JSON logs
						var str_html = html.toString();
						str_html = str_html.replace('[]', string);
						str_html = str_html.replace('[-]', JSON.stringify(number_logs));
						res.write(str_html);
						res.end();
					}

				});
			}
		});
	}


});

app.post('/*', function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	next(); // http://expressjs.com/guide.html#passing-route control
});

// ==================================================
// POST method for all actions
app.post(configs.path_API, middleware, function(req, res) {
	var action = req.body.action;
	console.log('I: --- ' + action + ' ---');
	switch (action) {
		case 'beam_key':
			// log('beam_key');
			beam_key_action(req, res);
			break;
		case 'room_list':
			// log('room_list');
			room_list_action(req, res);
			break;
		case 'key_list':
			// log('key_list');
			key_list_action(req, res);
			break;
		case 'access_room':
			// log('access_room');
			access_room_action(req, res);
			break;
		case 'create_room':
			// log('create_room');
			create_room_action(req, res);
			break;
		case 'create_key':
			// log('create_key');
			create_key_action(req, res);
			break;
		case 'delete_room':
			// log('delete_room');
			delete_room_action(req, res);
			break;
		case 'delete_key':
			// log('delete_key');
			delete_key_action(req, res);
			break;
		case 'change_status':
			// log('change_status');
			change_status_action(req, res);
			break;
		case 'change_time':
			// log('change_time');
			change_time_action(req, res);
			break;
		case 'test':
			// log('test');
			test(req, res);
			break;
		default:
			// log('There is NOT ' + action + ' action.');
			res.send('There is NOT ' + action + ' action.');
	}
});

// ==================================================
// beam key action function
function beam_key_action(req, res) {
	var key_id = req.body.key_id;
	if (key_id == undefined) {
		log('E: key_id is undefined', undefined, key_id, 'beam_key');
		res.send({});
	} else {
		room_from_key_id(key_id, function(callback) {
			var isFound = callback.found;
			var room = callback.room;
			// log('I: isFound= ' + isFound + ' room= ' + room);
			if (isFound) {
				keyDatabase.findOne({
					'key_id': key_id
				}, function(err, key) {
					if (err || key == null || key.secret_key == undefined) {
						res.send({});
					} else {
						var key_secret = key.secret_key;
						log('I: success', room.room_id, key_id, 'beam_key');
						res.send({
							'key_id': key_id,
							'key_name': key.name,
							'secret_key': key.secret_key,
							'thumbnail': room.thumbnail,
							'description': room.description,
							'latitude': room.latitude,
							'longitude': room.longitude
						});
					}
				});
			} else {
				res.send({});
			}

		});
	}
}

// ==================================================
// room list action function
function room_list_action(req, res) {
	roomsData = [];
	roomsDatabase.find({},
		function(err, rooms) {
			if (!err) {;
				rooms.forEach(function(room) {
					var json = {
						'room_id': room.room_id,
						'room_name': room.name,
						'status': room.status,
						'key_amount': room.keys.length,
						'thumbnail': room.thumbnail,
						'description': room.description,
						'latitude': room.latitude,
						'longitude': room.longitude
					};
					roomsData.push(json);
				});
			} else {
				log('E: ' + err, undefined, undefined, 'room_list');
			}
			log('I: success', undefined, undefined, 'room_list');
			res.send(roomsData);
		}
	);
}

// ==================================================
// key list action function
function key_list_action(req, res) {
	var room_id = req.body.room_id;
	if (room_id == undefined) {
		log('E: room_id is undefined', room_id, undefined, 'key_list');
		res.send([]);
	} else {
		var keysData = [];
		roomsDatabase.findOne({
				'room_id': room_id
			},
			function(err, room) {
				if (!err && room != null && room != undefined) {
					var ObjectId = room.keys;
					// console.log(room);
					if (ObjectId.length < 1) {
						res.send(keysData);
					};
					var countId = 0;
					ObjectId.forEach(function(id) {
						// console.log(id);
						keyDatabase.findOne({
								'_id': id
							},
							function(err, key) {
								countId++;
								// console.log(countId);
								if (!err) {
									// var count = 0;
									// console.log(key);
									if (key != null) {
										var json = {
											'key_id': key.key_id,
											'key_name': key.name,
											'status': key.status,
											'time_start': key.time_start,
											'time_end': key.time_end,
											'time_type': key.time_type
										};
										keysData.push(json);
										// count++;
										if (countId > ObjectId.length || countId == ObjectId.length) {
											log('I: success', room_id, undefined, 'key_list');
											// console.log(keysData.length);
											var sortedData = _.sortBy(keysData, function(keyF){
												return keyF.key_id;
											});
											res.send(sortedData);
										}
									}

								} else {
									log('E: ' + err, room_id, undefined, 'key_list');
									res.send(keysData);
								}

							});
					});
				} else {
					log('E: ' + err, room_id, undefined, 'key_list');
					res.send(keysData);
				}
			}
		);
	}
}

// ==================================================
// access room action function
function access_room_action(req, res) {
	var room_id = req.body.room_id;
	var token = req.body.token;
	var key_id = req.body.key_id;
	if (room_id == undefined || token == undefined || key_id == undefined) {
		log('E: room_id, key_id or token are undefined', room_id, key_id, 'access_room');
		res.send({
			'response': false
		});
	} else {
		roomsDatabase.findOne({
			'room_id': room_id
		}, function(err, room) {
			if (err || room == null || room.status == false) {
				log('I: room = null or room status = false', room_id, key_id, 'access_room');
				res.send({
					'response': false
				});
			} else {
				keyDatabase.findOne({
					'key_id': key_id
				}, function(err, key) {
					if (err || key == null || key.status == false) {
						log('I: key = null or key status == false', room_id, key_id, 'access_room');
						res.send({
							'response': false
						});
					} else {
						is_key_in_room(key.key_id, room_id, function(isKeyInRoom) {
							check_key_time(key, function(isEnableThisTime) {
								if (!isKeyInRoom || !isEnableThisTime) {
									log('I: key is not in room or time check is false', room_id, key_id, 'access_room');
									res.send({
										'response': false
									});
								} else {
									generate_totp_array(key.secret_key, function(array_pass) {
										var isSent = false;
										check_totp(token, array_pass, function(isValid) {
											if (!isSent) {
												isSent = true;
												console.log('I: access_room was ' + isValid + ' ' + room_id + ' ' + key_id);
												log('I: access_room was ' + isValid, room_id, key_id, 'access_room');
												res.send({
													'response': isValid
												});
											}
										});
									});
								}
							});
						});

					}
				});
			}
		});
	}
}

// ==================================================
// create room action function
function create_room_action(req, res) {
	room_index(function(room_index) {
		var room_id = room_index;
		var room_name = req.body.room_name;
		var status = req.body.status;
		var thumbnail = req.body.thumbnail;
		var description = req.body.description;
		var latitude = req.body.latitude;
		var longitude = req.body.longitude;
		var roomJson = {
			'room_id': room_id,
			'name': room_name,
			'keys': [],
			'status': status,
			'thumbnail': thumbnail,
			'description': description,
			'latitude': latitude,
			'longitude': longitude
		};
		if (room_name == undefined || status == undefined || thumbnail == undefined || description == undefined || latitude == undefined || longitude == undefined) {
			log('E: undefined some parameter', room_id, undefined, 'create_room');
			res.send({
				'response': false
			});
		} else {
			var roomObject = new roomsDatabase(roomJson);
			roomObject.save(function(err) {
				if (err) {
					log('E: error saving room', room_id, undefined, 'create_room');
					res.send({
						'response': false
					});
				} else {
					log('I: success', room_id, undefined, 'create_room');
					res.send({
						'response': true
					});
				}
			});
			updateRoomIndex();
		}
	});
}

// ==================================================
// create key action function
function create_key_action(req, res) {
	var key_length = configs.totp_generate_key_length;
	var key_name = req.body.key_name;
	var room_id = req.body.room_id;
	var status = req.body.status;
	var time_start = req.body.time_start;
	var time_end = req.body.time_end;

	// time type 
	// 0 = no time check, 1= no repeat, 2 = everyday, 3 = every week
	var time_type = req.body.time_type;
	if (key_name == undefined || room_id == undefined || status == undefined || time_start == undefined || time_end == undefined || time_type == undefined || time_type < 0 || time_type > 3) {
		log('E: undefined some parameter', room_id, undefined, 'create_key');
		res.send({
			'response': false
		});
	} else {
		key_index(function(key_index) {
			var keyJson = {
				'key_id': key_index,
				'name': key_name,
				'secret_key': totpGenerator.generate_key_time_base(key_length, false),
				'status': status,
				'time_start': time_start,
				'time_end': time_end,
				'time_type': time_type
			};

			var keyObject = new keyDatabase(keyJson);
			keyObject.save(function(err) {
				if (err) {
					log('E: error saving key', room_id, key_index, 'create_key');
					res.send({
						'response': false
					});
				} else {
					log('I: success create key', room_id, key_index, 'create_key');
					var keyObjectId = keyObject.id;
					roomsDatabase.findOne({
						'room_id': room_id
					}, function(err, room) {
						if (err || room == null) {
							log('E: error adding key to room', room_id, key_index, 'create_key');
							res.send({
								'response': false
							});
						} else {
							room.keys.push(keyObjectId);
							room.save(function(err) {
								if (err) {
									log('E: error saving update room', room_id, key_index, 'create_key');
									res.send({
										'response': false
									});
								} else {
									log('I: success adding key', room_id, key_index, 'create_key');
									res.send({
										'response': true
									});
								}
							});
						}
					});
				}
			});
		});
		updateKeyIndex();
	}
}

// ==================================================
// delete room action function
function delete_room_action(req, res) {
	var room_id = req.body.room_id;
	var ObjectId = [];
	if (room_id == undefined) {
		log('E: room_id is undefined', room_id, undefined, 'delete_room');
		res.send({
			'response': false
		});
	} else {
		roomsDatabase.find({
			'room_id': room_id
		}, function(err, rooms) {
			if (!err) {
				var count = 0;
				rooms.forEach(function(room) {
					ObjectId = room.keys;
					count++;

					if (count == rooms.length || count > rooms.Listening) {
						roomsDatabase.remove({
							'room_id': room_id
						}, function(err) {
							if (!err) {
								var count_object = 0;
								var keys_size = ObjectId.length;
								//loop to remove each key in room
								if (ObjectId.length < 1) {
									log('I: success delete room', room_id, undefined, 'delete_room');
									res.send({
										'response': true
									});
								} else {
									ObjectId.forEach(function(err, id) {
										count_object++;
										keyDatabase.findOneAndRemove({
											'key_id': id
										}, function(err) {
											if (!err) {
												if (count_object == keys_size) {
													log('I: success delete keys in room', room_id, id, 'delete_room');
													res.send({
														'response': true
													});
												}
											} else {
												log('E: ' + err, room_id, id, 'delete_room');
												res.send({
													'response': false
												});
											}
										});
									});
								}

							} else {
								log('E: ' + err, room_id, undefined, 'delete_room');
								res.send({
									'response': false
								});
							}
						});
					}
				});
			} else {
				log('E: ' + err, room_id, undefined, 'delete_room');
				res.send({
					'response': false
				});
			}
		});
	}
}

// ==================================================
// delect key action function
function delete_key_action(req, res) {
	var key_id = req.body.key_id;
	if (key_id == undefined) {
		log('E: key_id is undefined', undefined, key_id, 'delete_key');
		res.send({
			'response': false
		});
	} else {
		var key_object_id = '';
		keyDatabase.findOne({
			'key_id': key_id
		}, function(err, key) {
			if (key != null) {
				key_object_id = key.id;
				keyDatabase.remove({
					'key_id': key_id
				}, function(err) {
					if (!err) {
						roomsDatabase.update({
							'keys': key_object_id
						}, {
							$pull: {
								'keys': key_object_id
							}
						}, function(err) {
							log('I: success delete key', undefined, key_id, 'delete_key');
							res.send({
								'response': true
							});
						});
					} else {
						log('E: ' + err, undefined, key_id, 'delete_key');
						res.send({
							'response': false
						});
					}
				});
			} else {
				log('E: ' + err, undefined, key_id, 'delete_key');
				res.send({
					'response': false
				});
			}
		});
	}
}

// ==================================================
// change status action function
function change_status_action(req, res) {
	var room_id = req.body.room_id;
	var key_id = req.body.key_id;
	var status = req.body.status;

	if (status == undefined) {
		log("status is undefined", room_id, key_id, 'change_status');
		res.send({
			"response": false
		});
	} else {
		if (room_id != undefined && key_id == undefined) {
			change_room_status(room_id, status, function(err) {
				if (!err) {
					log('I: change status room to ' + status, room_id, key_id, 'change_status');
					res.send({
						'response': true
					});
				} else {
					log('E: error on changing room status', room_id, key_id, 'change_status');
					res.send({
						'response': false
					});
				}
			});
		} else if (key_id != undefined && room_id == undefined) {
			change_key_status(key_id, status, function(err) {
				if (!err) {
					log('I: change status key to ' + status, room_id, key_id, 'change_status');
					res.send({
						'response': true
					});
				} else {
					log('E: error on changing key status', room_id, key_id, 'change_status');
					res.send({
						'response': false
					});
				}
			});
		} else if (key_id == undefined && room_id == undefined) {
			log('E: undefined some parameter', room_id, key_id, 'change_status');
			res.send({
				'response': false
			});
		} else if (key_id != undefined && room_id != undefined) {
			log('E: undefined some parameter', room_id, key_id, 'change_status');
			res.send({
				'response': false
			});
		} else {
			log('E: undefined some parameter', room_id, key_id, 'change_status');
			res.send({
				'response': false
			});
		}
	}
}

// ==================================================
// change time action function
function change_time_action(req, res) {
	var key_id = req.body.key_id;
	var time_start = req.body.time_start;
	var time_end = req.body.time_end;
	var time_type = req.body.time_type;
	if (key_id == undefined || time_start == undefined || time_end == undefined || time_type < 0 || time_type > 3) {
		log('E: undefined some parameter', undefined, key_id, 'change_time');
		res.send({
			'response': false
		});
	} else {
		change_key_time(key_id, time_start, time_end, time_type, function(err) {
			if (!err) {
				log('I: success change key time', undefined, key_id, 'change_time');
				res.send({
					'response': true
				});
			} else {
				log('E: ' + err, undefined, key_id, 'change_time');
				res.send({
					'response': false
				});
			}
		});
	}

}

// ==================================================
// check totp function
function check_totp(token, array_pass, callback) {
	var size = array_pass.length;
	var count = 0;
	array_pass.forEach(function(one_pass) {
		count++;
		// console.log('I: ' + token + ' | ' + one_pass);
		if (token == one_pass) {
			// console.log('I: token is valid');
			callback(true);
		} else if (count == size || count > size) {
			callback(false);
		}
	});
}

function check_key_time(key, callback) {
	var type = key.time_type;
	var startDate = new Date(key.time_start);
	var endDate = new Date(key.time_end);
	var currentDate = new Date();
	console.log(key.time_start);
	console.log(key.time_end);
	
	// no time check
	if (type == 0) {
		callback(true);
	}
	// no repeat
	else if (type == 1) {
		console.log('start : ' + startDate);
		console.log('current : ' + currentDate);
		console.log('end : ' + endDate);
		if (startDate <= currentDate && endDate > currentDate) {
			callback(true);
		} else callback(false);
	}
	// everyday
	else if (type == 2) {
		var startT2 = new Date();
		startT2.setHours(startDate.getHours());
		startT2.setMinutes(startDate.getMinutes());
		startT2.setSeconds(startDate.getSeconds());
		var endT2 = new Date();
		endT2.setHours(startDate.getHours());
		endT2.setMinutes(startDate.getMinutes());
		endT2.setSeconds(startDate.getSeconds());
		if (startT2 <= currentDate && endT2 > currentDate) {
			callback(true);
		} else callback(false);
	}
	// every week
	else if (type == 3) {
		var startT3 = new Date();
		startT3.setHours(startDate.getHours());
		startT3.setMinutes(startDate.getMinutes());
		startT3.setSeconds(startDate.getSeconds());
		var endT2 = new Date();
		endT2.setHours(startDate.getHours());
		endT2.setMinutes(startDate.getMinutes());
		endT2.setSeconds(startDate.getSeconds());
		if (startT3 <= currentDate && endT2 > currentDate && startDate.getDay() == currentDate.getDay()) {
			callback(true);
		} else callback(false);
	}
}

// ==================================================
// generate totp from 
// time = intervaltime
// step = time step
// returen totp array in callback
function generate_totp_array(secret_key, callback) {
	// default from config
	var time_step = configs.totp_time_step;
	var intervaltime = configs.totp_interval_time;
	var current_time = parseInt(new Date().getTime() / 1000);
	var array_pass = [];
	for (var i = time_step; i >= 1; i--) {
		array_pass.push(totpGenerator.totp({
			key: secret_key,
			time: (current_time + (intervaltime * i)),
			intervaltime: intervaltime
		}));
	}

	array_pass.push(totpGenerator.totp({
		key: secret_key,
		time: current_time,
		intervaltime: intervaltime
	}));
	for (var i = 1; i <= time_step; i++) {

		array_pass.push(totpGenerator.totp({
			key: secret_key,
			time: (current_time - (intervaltime * i)),
			intervaltime: intervaltime
		}));
	}

	callback(array_pass);
}

// ==================================================
// return true if key_id was in that room_id functions
function is_key_in_room(key_id, room_id, callback) {
	room_from_key_id(key_id, function(response) {
		var room = response.room;
		var isFound = response.found;

		if (isFound == true && room != null && room != undefined && room.room_id == room_id) {
			console.log('true');
			callback(true);
		} else {
			console.log('false');
			callback(false);
		}
	});
}

// ==================================================
// get room from key id
// return { 'room': roomObject , 'found': boolean}
function room_from_key_id(key_id, callback) {
	if (key_id != undefined && key_id != null) {
		keyDatabase.findOne({
			'key_id': key_id
		}, function(err, key) {
			// console.log(key);
			if (key == null || key == undefined) {
				log('E: can not find any key id ' + key_id);
				callback({
					'room': undefined,
					'found': false
				});
			} else {
				roomsDatabase.findOne({
					'keys': key.id
				}, function(err, rooms) {
					if (rooms == null || key == undefined) {
						log('E: can not find any room from ' + key.id);
						callback({
							'room': undefined,
							'found': false
						});
					} else {
						log('I: found room');
						callback({
							'room': rooms,
							'found': true
						});
					}
				});
			}
		});
	}
}

// ==================================================
// get key from room id
function keys_from_room_id(room_id, callback) {
	var keys_objects = [];
	roomsDatabase.find({
		'room_id': room_id
	}, function(err, rooms) {
		if (rooms.length < 1) {
			log('E: room id ' + room_id + ' does not exist');
			callback(keys_objects);
		} else {
			rooms.forEach(function(room) {
				var ObjectId = room.keys;
				ObjectId.forEach(function(id) {
					keyDatabase.find({
							_id: id
						},
						function(err, keys) {
							if (!err) {
								var count = 0;
								keys.forEach(function(key) {
									keys_objects.push(key);
									count++;
									if (count == keys.length) {
										callback(keys_objects);
									};
								});
							} else {
								log('E: ' + err);
								callback(keys_objects);
							}
						});
				});
			});
		}
	});
}

// ==================================================
// change room status functions
function change_room_status(room_id, status, callback) {
	roomsDatabase.update({
			'room_id': room_id
		}, {
			$set: {
				'status': status
			}
		},
		function(err) {
			callback(err);
		});
}

// ==================================================
// change key status functions
function change_key_status(key_id, status, callback) {
	keyDatabase.update({
			'key_id': key_id
		}, {
			$set: {
				'status': status
			}
		},
		function(err) {
			callback(err);
		});
}

// ==================================================
// change key time
function change_key_time(key_id, time_start, time_end, time_type, callback) {
	keyDatabase.update({
			'key_id': key_id
		}, {
			$set: {
				'time_start': time_start,
				'time_end': time_end,
				'time_type': time_type
			}
		},
		function(err) {
			callback(err);
		});
}

// ==================================================
// room index functions
function room_index(callback) {
	indexDatabase.find({
		'index': 'index'
	}, function(err, indexs) {
		if (err) {
			log('E: error finding index');
		} else {
			indexs.forEach(function(index) {
				callback(index.room_index);
			});
		}
	});
}

// ==================================================
// key index functions
function key_index(callback) {
	indexDatabase.find({
		'index': 'index'
	}, function(err, indexs) {
		if (err) {
			log('E: error finding index');
		} else {
			indexs.forEach(function(index) {
				callback(index.key_index);
			});
		}
	});
}

// ==================================================
// room index update functions
function updateRoomIndex() {
	indexDatabase.update({}, {
		$inc: {
			'room_index': 1
		}
	}, function(err) {
		if (err) {
			log('E: error updating room index');
		} else {
			log('I: Room index is increased');
		}
	});
}

// ==================================================
// key index update functions
function updateKeyIndex() {
	indexDatabase.update({}, {
		$inc: {
			'key_index': 1
		}
	}, function(err) {
		if (err) {
			log('E: error updating key index');
		} else {
			log('I: Key index is increased');
		}
	});
}

// ==================================================
// remove all database
// index Database , room Database, key Database
function removeDatabase() {
	indexDatabase.remove({}, function(err) {
		log('I: ' + index_table_name + ' removed');
		setUpIndex();
	});
	roomsDatabase.remove({}, function(err) {
		log('I: ' + room_table_name + ' removed');
	});
	keyDatabase.remove({}, function(err) {
		log('I: ' + key_table_name + ' removed');
	});
}

// ==================================================
// set up index
function setUpIndex() {
	log('I: set up Index Data');
	var indexJson = {
		'index': 'index',
		'room_index': 0,
		'key_index': 0
	};
	var indexObject = new indexDatabase(indexJson);
	indexObject.save(function(err) {
		if (err) log('E: ' + err);
	});
}

function test(req, res) {
	res.send('nothing');
}

// removeDatabase();
// debugLog.remove_all_log();

function log(message, room_id, key_id, action) {
	debugLog.log(message, room_id, key_id, action);
}