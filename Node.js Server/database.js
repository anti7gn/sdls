
var mongoose = require('mongoose');

var Schemas = {}

// ==================================================
/*
	Index table
	running for room's index and key's index
*/
var indexSchema = mongoose.Schema({
	index: String,
	room_index: Number,
	key_index: Number
});

// ==================================================
/*
	Room table
*/
var roomSchema = mongoose.Schema({
	room_id: Number,
	name: String,
	keys: [mongoose.Schema.Types.ObjectId],
	status: Boolean,
	thumbnail: String,
	description: String,
	latitude: Number,
	longitude: Number
});

// ==================================================
/*
	Key table
*/
var keySchema = mongoose.Schema({
	key_id: Number,
	name: String,
	secret_key: String,
	status: Boolean,
	time_start: Number,
	time_end: Number,
	time_type: Number
});

// ==================================================
/*
	Log table
*/
var logSchema = mongoose.Schema({
	time: Number,
	room_id: Number,
	key_id: Number,
	action: String,
	debug: String
});

Schemas.indexSchema = indexSchema;
Schemas.roomSchema = roomSchema;
Schemas.keySchema = keySchema;
Schemas.logSchema = logSchema;

module.exports = Schemas;