var configs = {}

configs.database_directory = 'mongodb://localhost/sdl_p2';

// ==================================================
//main path of API server
configs.path_API = '/sdl';
configs.path_debug_log = '/sdl/log';
configs.base_url_server = 'https://www.theometas.com:2407';
configs.enable_debug_log = true;

// ==================================================
configs.db_room_table_name = 'roomdb';
configs.db_key_table_name = 'keydb';
configs.db_index_table_name = 'indexdb';
configs.db_log_table_name = 'logdb';

// ==================================================
// totp configs
configs.totp_interval_time = 20;
configs.totp_time_step = 3;
configs.totp_generate_key_length = 10;

module.exports = configs;


