var crypto = require('crypto'),
	ezcrypto = require('ezcrypto').Crypto;

var totpGenerator = {}

totpGenerator.hex_to_ascii = function(str) {
	// key is a string of hex
	// convert it to an array of bytes...
	var bytes = ezcrypto.util.hexToBytes(str);

	// bytes is now an array of bytes with character codes
	// merge this down into a string
	var ascii_string = new String();

	for (var i = 0; i < bytes.length; i++) {
		ascii_string += String.fromCharCode(bytes[i]);
	}

	return ascii_string;
}

// speakeasy.ascii_to_hex(key)
// helper function to convert an ascii key to hex.
totpGenerator.ascii_to_hex = function(str) {
	var hex_string = '';

	for (var i = 0; i < str.length; i++) {
		hex_string += str.charCodeAt(i).toString(16);
	}

	return hex_string;
}

totpGenerator.generate_key_ascii = function(length, symbols) {
	if (!length) length = 32;

	var set = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';

	if (symbols) {
		set += '!@#$%^&*()<>?/[]{},.:;';
	}

	var key = '';

	for (var i = 0; i < length; i++) {
		key += set.charAt(Math.floor(Math.random() * set.length));
	}

	return key;
}

totpGenerator.generate_key_time_base = function(length, symbols) {
	var secret = totpGenerator.generate_key_ascii(length, symbols);
	var secretAndTime = secret + parseInt(new Date().getTime());
	return totpGenerator.ascii_to_md5(secretAndTime);
}

totpGenerator.ascii_to_md5 = function(message) {
	var md5sum = crypto.createHash('md5');
	md5sum.update(message);
	var encoded = md5sum.digest('hex');
	return encoded;
}

// generate totp
totpGenerator.totp = function(options) {
	// set vars
	var key = options.key;
	var time = options.time;
	var intervaltime = options.intervaltime || 60;
	// var length = options.length || 8;
	var encoding = options.encoding || 'ascii';

	// preprocessing: convert to ascii if it's not
	if (encoding == 'hex') {
		key = totpGenerator.hex_to_ascii(key);
	} else if (encoding == 'base32') {
		key = base32.decode(key);
	}

	var octet_array = new Array(8);
	var bytes = new Buffer(key);

	// init hmac with the key
	var hmac = crypto.createHmac('sha1', new Buffer(key));

	// create an octet array from the counter
	var cutTime = time - (time % intervaltime);
	// console.log('cutTime - '+cutTime);
	// console.log("cutTime "+ cutTime);
	var counter_temp = cutTime;

	for (var i = 0; i < 8; i++) {
		var i_from_right = 7 - i;

		// mask 255 over number to get last 8
		octet_array[i_from_right] = counter_temp & 255;

		// shift 8 and get ready to loop over the next batch of 8
		counter_temp = counter_temp >> 8;
	}

	// create a buffer from the octet array
	var counter_buffer = new Buffer(octet_array);

	// update hmac with the counterß
	hmac.update(counter_buffer);

	// get the digest in hex format
	var digest = hmac.digest('hex');

	// convert the result to an array of bytes
	var digest_bytes = ezcrypto.util.hexToBytes(digest);

	// get offset
	var offset = digest_bytes[19] & 0xf;

	// calculate bin_code (RFC4226 5.4)
	var bin_code = (digest_bytes[offset] & 0x7f) << 24 | (digest_bytes[offset + 1] & 0xff) << 16 | (digest_bytes[offset + 2] & 0xff) << 8 | (digest_bytes[offset + 3] & 0xff);

	bin_code = bin_code.toString();

	var code = totpGenerator.ascii_to_md5(bin_code);
	// console.log('code - '+code);
	return (code);
}

module.exports = totpGenerator;