package com.saranomy.sdl;

public class Key implements Comparable<Key>{
	public String id;
	public String title;
	public String secretKey;

	public String thumbnail;
	public String description;
	public double latitude;
	public double longitude;

	public long lastUse;

	public Key(String id, String title, String secretKey, String thumbnail, String description, String latitude, String longitude, String lastUse) {
		this.id = id;
		this.title = title;
		this.secretKey = secretKey;
		this.thumbnail = thumbnail;
		this.description = description;

		try {
			this.latitude = Double.parseDouble(latitude);
		} catch (Exception e) {
			this.latitude = 9000;
		}

		try {
			this.longitude = Double.parseDouble(longitude);
		} catch (Exception e) {
			this.longitude = 9000;
		}
		
		try {
			this.lastUse = Long.parseLong(lastUse);
		} catch (Exception e) {
			this.lastUse = System.currentTimeMillis();
		}
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Key) {
			Key key = (Key) object;
			if (key.secretKey.equals(secretKey))
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return id + MyConfig.keySeperator + title + MyConfig.keySeperator + secretKey + MyConfig.keySeperator + thumbnail + MyConfig.keySeperator + description + MyConfig.keySeperator + latitude + MyConfig.keySeperator + longitude + MyConfig.keySeperator + lastUse +  MyConfig.keySeperator;
	}

	@Override
	public int compareTo(Key another) {
		return (int) (another.lastUse - this.lastUse);
	}
}
