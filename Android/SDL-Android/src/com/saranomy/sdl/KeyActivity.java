package com.saranomy.sdl;

import java.nio.charset.Charset;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.nfc.NfcEvent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class KeyActivity extends Activity implements CreateNdefMessageCallback, OnNdefPushCompleteCallback {
	private Activity activity;
	private NfcAdapter nfcAdapter;
	private int position;
	private Key key;

	private ImageView activity_key_animation;
	private Timer timer;
	private int count = 0;
	private final int countLimit = 6;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_key);
		activity = this;

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);

		try {
			Bundle bundle = getIntent().getExtras();
			position = bundle.getInt("position");
			key = KeyManager.getInstance().getKeys().get(position);

			setTitle(key.title);
			ImageSize iconSize = new ImageSize(256, 256);
			DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY_STRETCHED).build();
			ImageLoader.getInstance().loadImage(key.thumbnail, iconSize, options, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					activity.getActionBar().setIcon(new BitmapDrawable(loadedImage));
					super.onLoadingComplete(imageUri, view, loadedImage);
				}
			});

			// start Callback
			nfcAdapter = NfcAdapter.getDefaultAdapter(this);
			if (nfcAdapter == null) {
				Toast.makeText(this, "NFC is not available", Toast.LENGTH_LONG).show();
				onBackPressed();
			}
			// Register callback
			nfcAdapter.setNdefPushMessageCallback(this, this);
			nfcAdapter.setOnNdefPushCompleteCallback(this, this);

		} catch (Exception e) {
			e.printStackTrace();
		}
		activity_key_animation = (ImageView) findViewById(R.id.activity_key_animation);
	}
	
	@Override
	protected void onPause() {
		timer.cancel();
		super.onPause();
	}
	
	@Override
	protected void onResume() {
		timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {
						count = (count + 1) % countLimit;
						Log.e("asdf", ""+count);
						switch (count) {
						case (0): {
							activity_key_animation.setImageResource(R.drawable.beam_0);
							break;
						}
						case (1): {
							activity_key_animation.setImageResource(R.drawable.beam_1);
							break;
						}
						case (2): {
							activity_key_animation.setImageResource(R.drawable.beam_2);
							break;
						}
						case (3): {
							activity_key_animation.setImageResource(R.drawable.beam_3);
							break;
						}
						case (4): {
							activity_key_animation.setImageResource(R.drawable.beam_4);
							break;
						}
						case (5): {
							activity_key_animation.setImageResource(R.drawable.beam_5);
							break;
						}
						}
					}
				});
			}
		}, 0, 1000);
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_key, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_discard: {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
			alertDialog.setMessage(getString(R.string.deletekey_title));
			alertDialog.setPositiveButton(getString(R.string.deletekey_delete), new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					try {
						KeyManager.getInstance().removeKey(key);
						KeyManager.getInstance().overwrite(getApplicationContext());
					} catch (Exception e) {
					}
					onBackPressed();
				}
			});
			alertDialog.setNegativeButton(getString(R.string.addkey_cancel), null);
			alertDialog.show();
			return true;
		}
		case R.id.action_about: {
			if (key != null) {
				Intent intent = new Intent(getApplicationContext(), KeyAboutActivity.class);
				intent.putExtra("position", position);
				startActivity(intent);
			}
			return true;
		}
		case android.R.id.home: {
			onBackPressed();
			return true;
		}
		}
		return false;
	}

	@Override
	public NdefMessage createNdefMessage(NfcEvent event) {
		NdefRecord mimeRecord = new NdefRecord(NdefRecord.TNF_MIME_MEDIA, "application/com.saranomy.sdl.KeyActivity".getBytes(Charset.forName("US-ASCII")), new byte[0], generateTOTP());
		NdefMessage msg = new NdefMessage(mimeRecord);
		return msg;
	}

	private byte[] generateTOTP() {
		try {
			return String.valueOf(key.id + MyConfig.keySeperator + CustomTOTP.generateTOTP(key.secretKey.getBytes(), System.currentTimeMillis(), 60)).getBytes(Charset.forName("US-ASCII"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ".".getBytes(Charset.forName("US-ASCII"));
	}

	@Override
	public void onNdefPushComplete(NfcEvent event) {
		new OnNdefPushCompleteTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	public class OnNdefPushCompleteTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			onBackPressed();
			super.onPostExecute(result);
		}
	}
}
