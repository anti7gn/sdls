package com.saranomy.sdl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.util.Log;

public class KeyManager {
	private static boolean load = false;
	private static KeyManager keyManager = null;
	private List<Key> keys;

	private KeyManager() {
	}

	public static KeyManager getInstance() {
		if (keyManager == null)
			keyManager = new KeyManager();
		return keyManager;
	}

	public void load(Context context) {
		keys = new ArrayList<Key>();
		// debug
		// keys.add(new Key("1000", "Test Key", "SECRET_KEY",
		// "http://3.bp.blogspot.com/-uljwcG6DUco/TjE4Ht4RdGI/AAAAAAAAFW0/mv9NMb9Fenc/s400/Google_Plus_Logo_ApexBlogger%2B%25283%2529.gif",
		// "<a href=\"http://www.google.com\">Google</a> is your friend. I don't know what the fuck should I type in this long description :D",
		// "13", "100"));
		// keys.add(new Key("1000", "Test Key", "SECRET_KEY",
		// "http://hollymania.com/wp-content/uploads/2013/12/f7.png",
		// "<a href=\"http://www.google.com\">Google</a> is your friend. I don't know what the fuck should I type in this long description :D",
		// "13", "100"));

		String loadFile = KeyLoader.loadKeys(context);
		if (loadFile.length() != 0) {
			String[] split = loadFile.split(MyConfig.keySeperator);
			Log.e("LOG", "split " + split.length);
			if (split.length % 8 == 0) {
				for (int i = 0; i < split.length; i += 8) {
					try {
						Log.e("LOG", "add " + split[i]);
						keys.add(new Key(split[i], split[i + 1], split[i + 2], split[i + 3], split[i + 4], split[i + 5], split[i + 6], split[i + 7]));
					} catch (Exception e) {
						// old version of key that doesn't match
						e.printStackTrace();
					}
				}
			}
		}
		Collections.sort(keys);
		load = true;
	}

	public void overwrite(Context context) throws Exception {
		if (!load)
			throw new Exception("Init key first");
		StringBuilder sb = new StringBuilder();
		for (Key key : keys) {
			sb.append(key.toString());
		}
		KeyLoader.saveKeys(context, sb.toString());
	}
	
	public void setLastUse(Context context, int position) throws Exception {
		keys.get(position).lastUse = System.currentTimeMillis();
		overwrite(context);
	}

	public boolean addKey(Key newKey) throws Exception {
		if (!load)
			throw new Exception("Init key first");
		return keys.add(newKey);
	}

	public boolean removeKey(Key oldKey) throws Exception {
		if (!load)
			throw new Exception("Init key first");
		return keys.remove(oldKey);
	}

	public List<Key> getKeys() throws Exception {
		if (!load)
			throw new Exception("Init key first");
		return keys;
	}
}
