package com.saranomy.sdl;

import com.google.android.gms.internal.ac;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HowToActivity extends Activity {
	private HowToActivity activity;

	private Button activity_howto_back;
	private Button activity_howto_next;

	private ImageView activity_howto_image;
	private TextView activity_howto_text;
	private int page = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_howto);
		activity = this;

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);

		activity_howto_back = (Button) findViewById(R.id.activity_howto_back);
		activity_howto_next = (Button) findViewById(R.id.activity_howto_next);
		activity_howto_image = (ImageView) findViewById(R.id.activity_howto_image);
		activity_howto_text = (TextView) findViewById(R.id.activity_howto_text);

		activity_howto_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (page == 0) {
					activity.onBackPressed();
				} else {
					page--;
					refresh();
				}
			}
		});
		activity_howto_next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (page == 2) {
					activity.onBackPressed();
				} else {
					page++;
					refresh();
				}
			}
		});
	}

	private void refresh() {
		if (page == 0) {
			activity_howto_image.setImageResource(R.drawable.howto_0);
			activity_howto_text.setText(R.string.howto_0);
			activity_howto_next.setText(R.string.howto_next);
		} else if (page == 1) {
			activity_howto_image.setImageResource(R.drawable.howto_1);
			activity_howto_text.setText(R.string.howto_1);
			activity_howto_next.setText(R.string.howto_next);
		} else {
			activity_howto_image.setImageResource(R.drawable.howto_2);
			activity_howto_text.setText(R.string.howto_2);
			activity_howto_next.setText(R.string.howto_gotit);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home: {
			onBackPressed();
			return true;
		}
		}
		return false;
	}
}
