package com.saranomy.sdl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class KeyAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private List<Key> keys;

	public KeyAdapter(MainActivity mainActivity, List<Key> keys) {
		inflater = (LayoutInflater) mainActivity.getLayoutInflater();
		this.keys = keys;
	}

	@Override
	public int getCount() {
		return keys.size();
	}

	@Override
	public Object getItem(int position) {
		return keys.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View contentView, ViewGroup arg2) {
		View view = contentView;
		final ViewHolder holder;
		if (view == null) {
			view = inflater.inflate(R.layout.list_key, null);
			holder = new ViewHolder();
			holder.list_key_title = (TextView) view.findViewById(R.id.list_key_title);
			holder.list_key_title.setText(keys.get(position).title);
			
			holder.list_key_lastuse = (TextView) view.findViewById(R.id.list_key_lastuse);
			SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss MMM dd, yyyy");
			holder.list_key_lastuse.setText(format.format(new Date(keys.get(position).lastUse)));

			holder.list_key_thumbnail = (ImageView) view.findViewById(R.id.list_key_thumbnail);
			DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true).build();
			ImageLoader.getInstance().displayImage(keys.get(position).thumbnail, holder.list_key_thumbnail, options);

			holder.list_key_description = (TextView) view.findViewById(R.id.list_key_description);
			holder.list_key_description.setText(Html.fromHtml(keys.get(position).description));
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		return view;
	}
	
	public static class ViewHolder {
		private ImageView list_key_thumbnail;
		private TextView list_key_title;
		private TextView list_key_lastuse;
		private TextView list_key_description;
		
	}
}
