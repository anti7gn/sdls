package com.saranomy.sdl;

import java.net.URLDecoder;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MainActivity extends Activity implements CreateNdefMessageCallback {
	private MainActivity activity;
	private ListView activity_main_list;
	private boolean finishProcessingNDEF = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		activity = this;
		
		syncViewById();
		init();
	}

	private void syncViewById() {
		activity_main_list = (ListView) findViewById(R.id.activity_main_list);

	}

	private void init() {
		KeyManager.getInstance().load(getApplicationContext());
		refreshKeyList();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).build();
		ImageLoader.getInstance().init(config);
	}

	public void refreshKeyList() {
		try {
			KeyAdapter adapter = new KeyAdapter(this, KeyManager.getInstance().getKeys());
			activity_main_list.setAdapter(adapter);
			activity_main_list.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					try {
						// set most recent to this key
						KeyManager.getInstance().setLastUse(MainActivity.this, position);

						Intent intent = new Intent(getApplicationContext(), KeyActivity.class);
						intent.putExtra("position", position);
						startActivity(intent);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_howto: {
			startActivity(new Intent(activity, HowToActivity.class));
		}
		}
		return false;
	}

	@Override
	public NdefMessage createNdefMessage(NfcEvent arg0) {
		Toast.makeText(getApplicationContext(), "Select key to use", Toast.LENGTH_SHORT).show();
		return null;
	}

	@Override
	public void onResume() {
		KeyManager.getInstance().load(getApplicationContext());
		refreshKeyList();
		// Check to see that the Activity started due to an Android Beam
		if (!finishProcessingNDEF && NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
			processInComingNDEF(getIntent());
			finishProcessingNDEF = true;
		}
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	public void onNewIntent(Intent intent) {
		// onResume gets called after this to handle the intent
		setIntent(intent);
	}

	public void processInComingNDEF(Intent intent) {
		try {
			Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			NdefMessage msg = (NdefMessage) rawMsgs[0];
			final String inComingMessage = new String(msg.getRecords()[0].getPayload());
			// WARNING: incoming key_name should not use | in the context
			String key_name = (inComingMessage.split(MyConfig.keySeperator))[1];
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
			alertDialog.setCancelable(false);
			alertDialog.setTitle(getString(R.string.addkey_title));
			alertDialog.setMessage(getString(R.string.addkey_description) + " " + key_name + " " + getString(R.string.addkey_description_1));
			alertDialog.setPositiveButton(getString(R.string.addkey_add), new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					KeyManager keyManager = KeyManager.getInstance();
					try {
						String[] split = inComingMessage.split(MyConfig.keySeperator);
						keyManager.addKey(new Key(split[0], split[1], split[2], split[3], URLDecoder.decode(split[4], "UTF-8"), split[5], split[6], String.valueOf(System.currentTimeMillis())));
						keyManager.overwrite(getApplicationContext());
					} catch (Exception e) {
						e.printStackTrace();
					}
					refreshKeyList();
				}
			});
			alertDialog.setNegativeButton(getString(R.string.addkey_cancel), new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			alertDialog.show();
		} catch (Exception e) {
		}
	}
}
