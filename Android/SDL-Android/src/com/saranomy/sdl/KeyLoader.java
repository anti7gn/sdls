package com.saranomy.sdl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.util.Log;

public class KeyLoader {
	public static boolean saveKeys(Context context, String data) {
		try {
			FileOutputStream fos = context.openFileOutput("k", Context.MODE_PRIVATE);
			fos.write(data.getBytes());
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static String loadKeys(Context context) {
		try {
			FileInputStream fis = context.openFileInput("k");
			InputStreamReader inputStreamReader = new InputStreamReader(fis);
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				sb.append(line);
			}
			Log.e("LOG", sb.toString());
			return sb.toString();
		} catch (Exception e) {
		}
		return "";
	}
}
