package com.saranomy.sdl;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class KeyAboutActivity extends Activity {
	private KeyAboutActivity activity;
	private Key key;
	private TextView activity_key_about_title;
	private TextView activity_key_about_description;
	private Button activity_key_about_open_on_maps;
	private GoogleMap map;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_key_about);
		activity = this;

		try {
			Bundle bundle = getIntent().getExtras();
			int position = bundle.getInt("position");
			key = KeyManager.getInstance().getKeys().get(position);

			ImageSize iconSize = new ImageSize(256, 256);
			DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY_STRETCHED).build();
			ImageLoader.getInstance().loadImage(key.thumbnail, iconSize, options, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					activity.getActionBar().setIcon(new BitmapDrawable(loadedImage));
					super.onLoadingComplete(imageUri, view, loadedImage);
				}
			});
		} catch (Exception e) {
		}
		syncViewById();
	}

	private void syncViewById() {
		// assume that key is not null

		activity_key_about_title = (TextView) findViewById(R.id.activity_key_about_title);
		activity_key_about_description = (TextView) findViewById(R.id.activity_key_about_description);
		activity_key_about_open_on_maps = (Button) findViewById(R.id.activity_key_about_open_on_maps);

		activity_key_about_title.setText(key.title);
		activity_key_about_description.setText(Html.fromHtml(key.description));
		activity_key_about_description.setMovementMethod(LinkMovementMethod.getInstance());
		activity_key_about_open_on_maps.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("geo:" + key.latitude + "," + key.longitude));
				startActivity(intent);
			}
		});

		try {
			map = ((MapFragment) getFragmentManager().findFragmentById(R.id.activity_key_about_map)).getMap();
			map.getUiSettings().setAllGesturesEnabled(true);
			map.getUiSettings().setCompassEnabled(true);
			map.getUiSettings().setMyLocationButtonEnabled(true);

			LatLng position = new LatLng(key.latitude, key.longitude);
			CameraPosition cameraPosition = new CameraPosition(position, 15, 0, 0);
			map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 100, null);
			map.addMarker(new MarkerOptions().position(position).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_action_accounts)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
