package com.saranomy.sdl;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class CustomTOTP {
	public static String generateTOTP(byte[] key, long currentTime,
			int intervalCut) throws NoSuchAlgorithmException,
			InvalidKeyException, UnsupportedEncodingException {
		long cutTime = (currentTime / 1000)
				- ((currentTime / 1000) % intervalCut);
		byte[] msg = ByteBuffer.allocate(8).putLong(cutTime).array();
		Mac hmac = Mac.getInstance("HmacSHA1");
		SecretKeySpec macKey = new SecretKeySpec(key, "RAW");
		hmac.init(macKey);
		byte[] hash = hmac.doFinal(msg);
		int offset = hash[hash.length - 1] & 0xf;
		int binary = ((hash[offset] & 0x7f) << 24)
				| ((hash[offset + 1] & 0xff) << 16)
				| ((hash[offset + 2] & 0xff) << 8) | (hash[offset + 3] & 0xff);

		String result = String.valueOf(binary);
		byte[] bytesOfMessage = result.getBytes("UTF-8");
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] thedigest = md.digest(bytesOfMessage);
		return bytesToHex(thedigest);
	}
	
	private static char[] hexArray = "0123456789abcdef".toCharArray();
	private static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
}
