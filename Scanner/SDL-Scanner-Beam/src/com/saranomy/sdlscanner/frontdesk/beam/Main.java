package com.saranomy.sdlscanner.frontdesk.beam;

/**
 * Author: Saran Siripuekpong 5310546561
 * 
 * Updated: 5/26/2014
 */

public class Main {
	public static void main(String[] args) {
		/**
		 * Accept only String[]:keyData at length 7
		 * 
		 * Start ACR122U scanner to beam keyData as a NDEF Message to available
		 * NFC device
		 */
		if (args.length == 7) {
			BeamKeyDialog dialog = new BeamKeyDialog(args);
			dialog.open();
		} else {
			System.out.println(MyConfig.ARGS_ERROR);
		}
	}
}