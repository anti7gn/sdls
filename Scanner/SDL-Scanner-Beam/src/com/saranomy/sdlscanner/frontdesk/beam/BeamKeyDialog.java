package com.saranomy.sdlscanner.frontdesk.beam;

/**
 * Author: Saran Siripuekpong 5310546561
 * 
 * Updated: 5/26/2014
 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.nfctools.NfcAdapter;
import org.nfctools.examples.TerminalUtils;
import org.nfctools.llcp.LlcpConnectionManager;
import org.nfctools.llcp.LlcpConnectionManagerFactory;
import org.nfctools.llcp.LlcpConstants;
import org.nfctools.llcp.LlcpOverNfcip;
import org.nfctools.ndef.NdefListener;
import org.nfctools.ndef.Record;
import org.nfctools.ndef.mime.TextMimeRecord;
import org.nfctools.ndefpush.NdefPushFinishListener;
import org.nfctools.ndefpush.NdefPushLlcpService;
import org.nfctools.scio.Terminal;
import org.nfctools.scio.TerminalMode;

public class BeamKeyDialog {
	private String[] keyData;
	private JFrame frame;
	private JTextField status;
	private NdefListener ndefListener;
	private NdefPushLlcpService ndefPushLlcpService;
	private NfcAdapter nfcAdapter;
	private Terminal terminal;

	public BeamKeyDialog(String[] keyData) {
		this.keyData = keyData;
	}

	public void open() {
		frame = new JFrame(MyConfig.BEAM_KEY_DIALOG_TITLE);
		frame.addWindowListener(new WindowListener() {
			@Override
			public void windowOpened(WindowEvent event) {
			}

			@Override
			public void windowIconified(WindowEvent event) {
			}

			@Override
			public void windowDeiconified(WindowEvent event) {
			}

			@Override
			public void windowDeactivated(WindowEvent event) {
			}

			@Override
			public void windowClosing(WindowEvent event) {
				/**
				 * Close scanner before window is dead
				 */
				if (nfcAdapter != null) {
					nfcAdapter.stopListening();
					try {
						terminal.stopListening();
						terminal.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				System.exit(0);
			}

			@Override
			public void windowClosed(WindowEvent event) {
			}

			@Override
			public void windowActivated(WindowEvent event) {
			}
		});
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		status = new JTextField();
		status.setEditable(false);
		status.setFont(new Font("Verdana", Font.BOLD, 18));
		status.setHorizontalAlignment(JLabel.CENTER);
		frame.add(status, BorderLayout.CENTER);
		frame.pack();
		frame.setSize(400, 200);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
		frame.setVisible(true);

		/**
		 * Start Scanner to push keyData
		 */
		try {
			NdefPushContent(keyData);
		} catch (Exception e) {
			/**
			 * Handle error from hardware
			 */
			status.setText(MyConfig.BEAM_KEY_STATUS_ERROR);
			status.setBackground(MyConfig.BEAM_KEY_STATUS_COLOR_RED);
			e.printStackTrace();
		}
	}

	public void NdefPushContent(String[] keyData) throws Exception {
		status.setText(MyConfig.BEAM_KEY_STATUS_READY);
		ndefPushLlcpService = new NdefPushLlcpService(ndefListener);
		final ArrayList<Record> records = new ArrayList<Record>();

		/**
		 * Construct output data
		 */
		StringBuilder outputData = new StringBuilder();
		if (keyData.length > 0) {
			for (int i = 0; i < keyData.length - 1; i++) {
				outputData.append(keyData[i]).append(MyConfig.SEPERATOR);
			}
			outputData.append(keyData[keyData.length - 1]);
		}
		// "Will beam -> " + outputData.toString();
		records.add(new TextMimeRecord(MyConfig.BEAM_KEY_APP_MIME, outputData.toString()));
		addMessages(records, new NdefPushFinishListener() {
			@Override
			public void onNdefPushFinish() {
				/**
				 * Stop terminal and close dialog after Android receives Key
				 */
				status.setText(MyConfig.BEAM_KEY_STATUS_SUCCESS);
				frame.dispose();
				nfcAdapter.stopListening();
			}

			@Override
			public void onNdefPushFailed() {
				status.setText(MyConfig.BEAM_KEY_STATUS_ERROR);
				nfcAdapter.stopListening();
			}
		});
		LlcpOverNfcip llcpOverNfcip = new LlcpOverNfcip(new LlcpConnectionManagerFactory() {
			@Override
			protected void configureConnectionManager(LlcpConnectionManager connectionManager) {
				connectionManager.registerWellKnownServiceAccessPoint(LlcpConstants.COM_ANDROID_NPP, ndefPushLlcpService);
			}
		});
		terminal = TerminalUtils.getAvailableTerminal(MyConfig.TERMINAL_NAME);
		terminal.setNfcipConnectionListener(llcpOverNfcip);
		nfcAdapter = new NfcAdapter(terminal, TerminalMode.TARGET);
		nfcAdapter.setNfcipConnectionListener(llcpOverNfcip);
		nfcAdapter.startListening();
		// "Using: " + terminal.getTerminalName() + " Mode: " + terminalMode;
	}

	public void addMessages(Collection<Record> ndefRecords, NdefPushFinishListener finishListener) {
		ndefPushLlcpService.addMessages(ndefRecords, finishListener);
	}

	public String getTerminalName() {
		return terminal.getTerminalName();
	}
}