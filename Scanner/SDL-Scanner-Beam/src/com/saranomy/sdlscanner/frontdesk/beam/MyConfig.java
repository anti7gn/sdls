package com.saranomy.sdlscanner.frontdesk.beam;

/**
 * Author: Saran Siripuekpong 5310546561
 * 
 * Updated: 5/26/2014
 */

import java.awt.Color;

public class MyConfig {
	/**
	 * General
	 */
	public static final String SEPERATOR = "__";
	public static final String ARGS_ERROR = "Not enough args!";
	public static final String TERMINAL_NAME = "ACS ACR122 0";

	/**
	 * BeamKeyDialog
	 */
	public static final String BEAM_KEY_APP_MIME = "application/com.saranomy.sdl";
	public static final String BEAM_KEY_DIALOG_TITLE = "Beam Status";
	public static final String BEAM_KEY_STATUS_READY = "Ready to Beam";
	public static final String BEAM_KEY_STATUS_SUCCESS = "Success";
	public static final String BEAM_KEY_STATUS_ERROR = "Error, Try Again";
	public static final Color BEAM_KEY_STATUS_COLOR_RED = new Color(0xFFFF7F7F);
}
