package com.saranomy.sdlscanner.frontdesk;

/**
 * Author: Saran Siripuekpong 5310546561
 * 
 * Updated: 5/26/2014
 */

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Widget;
import org.json.JSONArray;
import org.json.JSONObject;

public class SystemController {
	private SystemController system;
	private Table tableRoom;
	private Table tableKey;
	private Button btnCreateRoom;
	private Button btnCreateKey;
	private Button btnToggleStatus;
	private Button btnDelete;
	private Button btnBeamKey;
	public Label lblStatus;

	private static JSONArray room_list;
	private static JSONArray key_list;

	private int room_id_select;
	private int key_id_select;
	private Widget select_item;
	private SelectMode selectingMode;

	private enum SelectMode {
		NONE, ROOM, KEY
	};

	public SystemController(Main gui) {
		system = this;
		tableRoom = gui.tableRoom;
		tableKey = gui.tableKey;
		btnCreateRoom = gui.btnCreateRoom;
		btnCreateKey = gui.btnCreateKey;
		btnToggleStatus = gui.btnToggleStatus;
		btnDelete = gui.btnDelete;
		btnBeamKey = gui.btnBeamKey;
		lblStatus = gui.lblStatus;
		setUpListener();

		refreshRoomList();
	}

	private void setUpListener() {
		btnCreateRoom.setEnabled(true);
		btnCreateKey.setEnabled(false);
		btnToggleStatus.setEnabled(false);
		btnDelete.setEnabled(false);
		btnBeamKey.setEnabled(false);

		btnCreateRoom.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblStatus.setText("Open create room dialog");
				try {
					CreateRoomDialog createRoomDialog = new CreateRoomDialog(system);
					createRoomDialog.open();
					btnCreateRoom.setEnabled(false);
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		});
		btnCreateKey.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				try {
					CreateKeyDialog window = new CreateKeyDialog(system, room_id_select);
					window.open();
					btnCreateKey.setEnabled(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnToggleStatus.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				/**
				 * Look what user selects (SelectMode) to toggle Room/Key status
				 */
				try {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("action", "change_status");
					if (selectingMode == SelectMode.ROOM) {
						jsonObject.put("room_id", room_id_select);
						lblStatus.setText("toggle room_id: " + room_id_select);
					} else if (selectingMode == SelectMode.KEY) {
						jsonObject.put("key_id", key_id_select);
						lblStatus.setText("toggle key_id: " + key_id_select);
					} else {
						return;
					}
					jsonObject.put("status", !Boolean.parseBoolean(select_item.getData("status").toString()));
					MyLog.d("req: " + jsonObject);
					String response = ConnectionHandler.doPostString(jsonObject.toString());
					MyLog.d("res: " + response);
					/**
					 * Change client display if it's successfully toggled
					 */
					if (new JSONObject(response).getBoolean("response")) {
						if (selectingMode == SelectMode.ROOM) {
							refreshRoomList();
						} else if (selectingMode == SelectMode.KEY) {
							refreshKeyList(key_id_select);
						}
						lblStatus.setText("Toggle success");
					} else {
						lblStatus.setText("Toggle failed");
					}
				} catch (Exception e) {
					lblStatus.setText("Error: Toggling");
					e.printStackTrace();
				}
			}
		});
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				try {
					/**
					 * Look what user selects (SelectMode) to delete Room/Key
					 */
					JSONObject jsonObject = new JSONObject();
					if (selectingMode == SelectMode.ROOM) {
						jsonObject.put("action", "delete_room");
						jsonObject.put("room_id", room_id_select);
						lblStatus.setText("deleting room_id: " + room_id_select);
					} else if (selectingMode == SelectMode.KEY) {
						jsonObject.put("action", "delete_key");
						lblStatus.setText("deleting key_id: " + key_id_select);
						jsonObject.put("key_id", key_id_select);
					} else {
						return;
					}
					MyLog.d("req: " + jsonObject);
					String response = ConnectionHandler.doPostString(jsonObject.toString());
					MyLog.d("res: " + response);
					if (new JSONObject(response).getBoolean("response") == true) {
						lblStatus.setText("Delete success");
					} else {
						lblStatus.setText("Delete failed");
					}
					refreshRoomList();
					refreshKeyList(key_id_select);
				} catch (Exception e) {
					lblStatus.setText("Error: Deleting...");
					e.printStackTrace();
				}
			}
		});
		btnBeamKey.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				/**
				 * Look if user selects any Key to start beaming
				 */
				if (selectingMode == SelectMode.KEY) {
					try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("action", "beam_key");
						jsonObject.put("key_id", key_id_select);
						MyLog.d("req: " + jsonObject);
						String response = ConnectionHandler.doPostString(jsonObject.toString());
						MyLog.d("res: " + response);
						JSONObject responseJSONObject = new JSONObject(response);
						String key_id = String.valueOf(responseJSONObject.getInt("key_id"));
						String key_name = responseJSONObject.getString("key_name");
						String secret_key = responseJSONObject.getString("secret_key");
						String thumbnail = responseJSONObject.getString("thumbnail");
						String description = responseJSONObject.getString("description");
						String latitude = String.valueOf(responseJSONObject.getDouble("latitude"));
						String longitude = String.valueOf(responseJSONObject.getDouble("longitude"));
						lblStatus.setText("Beam is ready");

						/**
						 *  Run com.saranomy.sdlscanner.frontdesk.beam.Main with keyData parameters
						 */
						String path = SystemController.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace(".jar", "-beam.jar").substring(1);
						String command = "java -jar " + path + " \"" + key_id + "\" \"" + key_name + "\" \"" + secret_key + "\" \"" + thumbnail + "\" \"" + description + "\" \"" + latitude + "\" \"" + longitude + "\"";
						MyLog.d(command);
						Runtime.getRuntime().exec(command);
					} catch (Exception e) {
						lblStatus.setText("Error: Cannot Beam this key");
						e.printStackTrace();
					}
				}
			}
		});
		tableRoom.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				selectingMode = SelectMode.ROOM;
				btnBeamKey.setEnabled(false);
				btnCreateRoom.setEnabled(true);
				btnCreateKey.setEnabled(true);
				btnToggleStatus.setEnabled(true);
				btnDelete.setEnabled(true);
				select_item = event.item;
				room_id_select = Integer.parseInt(event.item.getData("room_id").toString());
				refreshKeyList(room_id_select);
			}
		});
		tableKey.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				selectingMode = SelectMode.KEY;
				btnBeamKey.setEnabled(true);
				try {
					select_item = event.item;
					key_id_select = Integer.parseInt(event.item.getData("key_id").toString());
					lblStatus.setText("Selecting key_id: " + key_id_select);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void refreshRoomList() {
		btnCreateRoom.setEnabled(true);
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("action", "room_list");
			lblStatus.setText("Loading Rooms from server...");

			MyLog.d("req: " + jsonObject.toString());
			String response = ConnectionHandler.doPostString(jsonObject.toString());
			MyLog.d("res: " + response);
			room_list = new JSONArray(response);
			int roomLength = room_list.length();
			tableRoom.removeAll();
			for (int i = 0; i < roomLength; i++) {
				JSONObject roomObject = room_list.getJSONObject(i);
				TableItem tableItem = new TableItem(tableRoom, SWT.NONE);
				String room_id = String.valueOf(roomObject.getInt("room_id"));
				String room_name = roomObject.getString("room_name");
				String thumbnail = roomObject.getString("thumbnail");
				String description = roomObject.getString("description");
				String latitude = String.valueOf(roomObject.getDouble("latitude"));
				String longitude = String.valueOf(roomObject.getDouble("longitude"));
				String status = String.valueOf(roomObject.getBoolean("status"));
				String key_amount = String.valueOf(roomObject.getInt("key_amount"));

				tableItem.setText(new String[] { room_id, room_name, thumbnail, description, latitude, longitude, status, key_amount });
				tableItem.setData("room_id", roomObject.get("room_id").toString());
				tableItem.setData("status", roomObject.get("status").toString());
			}
			lblStatus.setText("All rooms are loaded");
		} catch (Exception e) {
			lblStatus.setText("Error: Cannot load any rooms");
			e.printStackTrace();
		}
	}

	public void refreshKeyList(int room_id) {
		try {
			lblStatus.setText("Loading Keys room_id: " + room_id_select);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("action", "key_list");
			jsonObject.put("room_id", room_id_select);
			MyLog.d("req: " + jsonObject);
			String response = ConnectionHandler.doPostString(jsonObject.toString());
			MyLog.d("res: " + response);
			key_list = new JSONArray(response);
			int keyLength = key_list.length();
			tableKey.removeAll();
			for (int i = 0; i < keyLength; i++) {
				JSONObject roomObject = key_list.getJSONObject(i);
				TableItem tableItem = new TableItem(tableKey, SWT.NONE);
				String key_id = String.valueOf(roomObject.getInt("key_id"));
				String key_name = roomObject.getString("key_name");
				String time_type = String.valueOf(roomObject.getInt("time_type"));
				String time_start = String.valueOf(roomObject.getLong("time_start"));
				String time_end = String.valueOf(roomObject.getLong("time_end"));
				String status = String.valueOf(roomObject.getBoolean("status"));

				tableItem.setText(new String[] { key_id, key_name, time_type, time_start, time_end, status });
				tableItem.setData("key_id", Integer.parseInt(roomObject.get("key_id").toString()));
				tableItem.setData("key_name", roomObject.get("key_name").toString());
				tableItem.setData("status", Boolean.parseBoolean(roomObject.get("status").toString()));
			}
			lblStatus.setText("Keys room_id: " + room_id_select + " are loaded");
		} catch (Exception e) {
			lblStatus.setText("Error: Cannot load any keys");
			e.printStackTrace();
		}
	}
}
