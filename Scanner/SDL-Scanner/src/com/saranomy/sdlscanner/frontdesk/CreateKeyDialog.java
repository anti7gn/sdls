package com.saranomy.sdlscanner.frontdesk;

/**
 * Author: Saran Siripuekpong 5310546561
 * 
 * Updated: 5/26/2014
 */

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.json.JSONObject;

public class CreateKeyDialog {
	protected Shell shell;
	private SystemController system;
	private int room_id;
	private Text txtKeyName;
	private Button btnCheckOnline;
	private Text txtTimeType;
	private Text txtTimeStart;
	private Text txtTimeEnd;
	private Label lblTimeType;
	private Label lblTimeStart;
	private Label lblTimeEnd;

	public CreateKeyDialog(SystemController system, int room_id) {
		this.system = system;
		this.room_id = room_id;
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 * 
	 * @wbp.parser.entryPoint
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(300, 220);
		shell.setText(MyConfig.CREATE_KEY_TITLE);

		Label lblName = new Label(shell, SWT.NONE);
		lblName.setBounds(10, 13, 40, 15);
		lblName.setText(MyConfig.CREATE_KEY_NAME);

		txtKeyName = new Text(shell, SWT.BORDER);
		txtKeyName.setBounds(77, 10, 197, 21);

		btnCheckOnline = new Button(shell, SWT.CHECK);
		btnCheckOnline.setBounds(77, 124, 197, 16);
		btnCheckOnline.setText(MyConfig.CREATE_KEY_STATUS);

		Button btnCreateKey = new Button(shell, SWT.NONE);
		btnCreateKey.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				try {
					/**
					 * Click Create Button
					 */
					String key_name = txtKeyName.getText().toString().trim();
					String time_type = txtTimeType.getText().toString().trim();
					String time_start = txtTimeStart.getText().toString().trim();
					String time_end = txtTimeEnd.getText().toString().trim();
					int iTime_type;
					long lTime_start, lTime_end;
					boolean status = btnCheckOnline.getSelection();

					/**
					 * Verify inputs
					 */
					if (key_name.length() == 0 || time_type.length() == 0 || time_start.length() == 0 || time_end.length() == 0)
						return;
					try {
						iTime_type = Integer.parseInt(time_type);
						lTime_start = Long.parseLong(time_start);
						lTime_end = Long.parseLong(time_end);
					} catch (NumberFormatException e) {
						return;
					}

					/**
					 * Do action = create_key
					 */
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("action", "create_key");
					jsonObject.put("key_name", key_name);
					jsonObject.put("room_id", room_id);
					jsonObject.put("status", status);
					jsonObject.put("time_start", lTime_start);
					jsonObject.put("time_end", lTime_end);
					jsonObject.put("time_type", iTime_type);
					MyLog.d("req: " + jsonObject);
					String response = ConnectionHandler.doPostString(jsonObject.toString());
					MyLog.d("res: " + response);
					if (new JSONObject(response).getBoolean("response")) {
						system.lblStatus.setText("Create key success");
					} else {
						system.lblStatus.setText("Create key failed");
					}

				} catch (Exception e) {
					system.lblStatus.setText("Error: Create key failed");
					e.printStackTrace();
				}
				system.refreshKeyList(room_id);
				system.refreshRoomList();
				shell.close();
			}
		});
		btnCreateKey.setBounds(118, 146, 75, 25);
		btnCreateKey.setText(MyConfig.CREATE_KEY_CREATE);

		Button btnCancel = new Button(shell, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.close();
			}
		});
		btnCancel.setBounds(199, 146, 75, 25);
		btnCancel.setText(MyConfig.CREATE_KEY_CANCEL);

		txtTimeType = new Text(shell, SWT.BORDER);
		txtTimeType.setBounds(77, 37, 197, 21);

		txtTimeStart = new Text(shell, SWT.BORDER);
		txtTimeStart.setBounds(77, 65, 197, 21);

		txtTimeEnd = new Text(shell, SWT.BORDER);
		txtTimeEnd.setBounds(77, 92, 197, 21);

		lblTimeType = new Label(shell, SWT.NONE);
		lblTimeType.setBounds(10, 43, 61, 15);
		lblTimeType.setText(MyConfig.CREATE_KEY_TIME_TYPE);

		lblTimeStart = new Label(shell, SWT.NONE);
		lblTimeStart.setText(MyConfig.CREATE_KEY_TIME_START);
		lblTimeStart.setBounds(10, 71, 61, 15);

		lblTimeEnd = new Label(shell, SWT.NONE);
		lblTimeEnd.setText(MyConfig.CREATE_KEY_TIME_END);
		lblTimeEnd.setBounds(10, 98, 61, 15);
	}
}
