package com.saranomy.sdlscanner.frontdesk;

/**
 * Author: Saran Siripuekpong 5310546561
 * 
 * Updated: 5/26/2014
 */

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class Main {
	public Shell shell;
	public Table tableRoom;
	public Table tableKey;
	public Button btnCreateRoom;
	public Button btnCreateKey;
	public Button btnToggleStatus;
	public Button btnDelete;
	public Button btnBeamKey;
	public Label lblStatus;

	/**
	 * Launch Frontdesk Scanner application.
	 */
	public static void main(String[] args) {
		try {
			Main window = new Main();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private static class ViewerLabelProvider extends LabelProvider {
		public Image getImage(Object element) {
			return super.getImage(element);
		}

		public String getText(Object element) {
			return super.getText(element);
		}
	}

	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		new SystemController(this);
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(800, 640);
		shell.setText(MyConfig.GUI_TITLE);
		shell.setLayout(new FormLayout());

		Composite composite = new Composite(shell, SWT.NONE);
		FormData fd_composite = new FormData();
		fd_composite.left = new FormAttachment(0, 10);
		composite.setLayoutData(fd_composite);
		composite.setLayout(new FillLayout(SWT.VERTICAL));

		tableRoom = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		tableRoom.setHeaderVisible(true);
		tableRoom.setLinesVisible(true);

		TableColumn tblclmnRoomId = new TableColumn(tableRoom, SWT.NONE);
		tblclmnRoomId.setToolTipText("");
		tblclmnRoomId.setWidth(80);
		tblclmnRoomId.setText(MyConfig.GUI_ROOM_ID);

		TableColumn tblclmnRoomName = new TableColumn(tableRoom, SWT.NONE);
		tblclmnRoomName.setWidth(120);
		tblclmnRoomName.setText(MyConfig.GUI_ROOM_NAME);

		TableColumn tblclmnThumbnail = new TableColumn(tableRoom, SWT.NONE);
		tblclmnThumbnail.setWidth(120);
		tblclmnThumbnail.setText(MyConfig.GUI_THUMBNAIL);

		TableColumn tblclmnDescription = new TableColumn(tableRoom, SWT.NONE);
		tblclmnDescription.setWidth(120);
		tblclmnDescription.setText(MyConfig.GUI_DESCRIPTION);

		TableColumn tblclmnLatitude = new TableColumn(tableRoom, SWT.NONE);
		tblclmnLatitude.setWidth(80);
		tblclmnLatitude.setText(MyConfig.GUI_LATITUDE);

		TableColumn tblclmnLongitude = new TableColumn(tableRoom, SWT.NONE);
		tblclmnLongitude.setWidth(80);
		tblclmnLongitude.setText(MyConfig.GUI_LONGITUDE);

		TableColumn tblclmnStatus = new TableColumn(tableRoom, SWT.NONE);
		tblclmnStatus.setWidth(80);
		tblclmnStatus.setText(MyConfig.GUI_STATUS);

		TableColumn tblclmnKey = new TableColumn(tableRoom, SWT.NONE);
		tblclmnKey.setWidth(80);
		tblclmnKey.setText(MyConfig.GUI_KEY_AMOUNT);

		tableKey = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		tableKey.setLinesVisible(true);
		tableKey.setHeaderVisible(true);

		TableColumn tblclmnKeyId = new TableColumn(tableKey, SWT.NONE);
		tblclmnKeyId.setWidth(80);
		tblclmnKeyId.setText(MyConfig.GUI_KEY_ID);

		TableColumn tblclmnKeyName = new TableColumn(tableKey, SWT.NONE);
		tblclmnKeyName.setWidth(120);
		tblclmnKeyName.setText(MyConfig.GUI_KEY_NAME);

		TableColumn tblclmnTimeMode = new TableColumn(tableKey, SWT.NONE);
		tblclmnTimeMode.setWidth(120);
		tblclmnTimeMode.setText(MyConfig.GUI_TIME_MODE);

		TableColumn tblclmnStart = new TableColumn(tableKey, SWT.NONE);
		tblclmnStart.setWidth(180);
		tblclmnStart.setText(MyConfig.GUI_TIME_START);

		TableColumn tblclmnEnd = new TableColumn(tableKey, SWT.NONE);
		tblclmnEnd.setWidth(180);
		tblclmnEnd.setText(MyConfig.GUI_TIME_END);

		TableColumn tblclmnKeyStatus = new TableColumn(tableKey, SWT.NONE);
		tblclmnKeyStatus.setWidth(80);
		tblclmnKeyStatus.setText(MyConfig.GUI_STATUS);

		btnCreateRoom = new Button(shell, SWT.NONE);
		fd_composite.top = new FormAttachment(btnCreateRoom, 6);
		FormData fd_btnCreateRoom = new FormData();
		fd_btnCreateRoom.top = new FormAttachment(0, 10);
		fd_btnCreateRoom.left = new FormAttachment(0, 10);
		btnCreateRoom.setLayoutData(fd_btnCreateRoom);
		btnCreateRoom.setText(MyConfig.GUI_CREATE_ROOM);

		btnCreateKey = new Button(shell, SWT.NONE);
		FormData fd_btnAddKey = new FormData();
		fd_btnAddKey.top = new FormAttachment(btnCreateRoom, 0, SWT.TOP);
		fd_btnAddKey.left = new FormAttachment(btnCreateRoom, 6);
		btnCreateKey.setLayoutData(fd_btnAddKey);
		btnCreateKey.setText(MyConfig.GUI_CREATE_KEY);

		btnToggleStatus = new Button(shell, SWT.NONE);
		FormData fd_btnEdit = new FormData();
		fd_btnEdit.top = new FormAttachment(btnCreateRoom, 0, SWT.TOP);
		fd_btnEdit.left = new FormAttachment(btnCreateKey, 6);
		btnToggleStatus.setLayoutData(fd_btnEdit);
		btnToggleStatus.setText(MyConfig.GUI_TOGGLE_STATUS);

		btnDelete = new Button(shell, SWT.NONE);
		FormData fd_btnDelete = new FormData();
		fd_btnDelete.top = new FormAttachment(btnCreateRoom, 0, SWT.TOP);
		fd_btnDelete.left = new FormAttachment(btnToggleStatus, 6);
		btnDelete.setLayoutData(fd_btnDelete);
		btnDelete.setText(MyConfig.GUI_DELETE);

		btnBeamKey = new Button(shell, SWT.NONE);
		fd_composite.right = new FormAttachment(btnBeamKey, 0, SWT.RIGHT);
		FormData fd_btnSetOutputKey = new FormData();
		fd_btnSetOutputKey.top = new FormAttachment(btnCreateRoom, 0, SWT.TOP);
		fd_btnSetOutputKey.right = new FormAttachment(100, -10);
		btnBeamKey.setLayoutData(fd_btnSetOutputKey);
		btnBeamKey.setText(MyConfig.GUI_BEAM_KEY);

		lblStatus = new Label(shell, SWT.NONE);
		fd_composite.bottom = new FormAttachment(lblStatus, -6);
		FormData fd_lblStatus = new FormData();
		fd_lblStatus.right = new FormAttachment(composite, 0, SWT.RIGHT);
		fd_lblStatus.bottom = new FormAttachment(100, -10);
		fd_lblStatus.left = new FormAttachment(0, 10);
		lblStatus.setLayoutData(fd_lblStatus);
		formToolkit.adapt(lblStatus, true, true);
		lblStatus.setText(MyConfig.GUI_STATUS);
	}
}
