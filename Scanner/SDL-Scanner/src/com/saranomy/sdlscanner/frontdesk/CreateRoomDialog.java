package com.saranomy.sdlscanner.frontdesk;

/**
 * Author: Saran Siripuekpong 5310546561
 * 
 * Updated: 5/26/2014
 */

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.json.JSONObject;

public class CreateRoomDialog {

	protected Shell shell;
	private SystemController system;
	private Text txtRoomName;
	private Text txtThumbnail;
	private Text txtDescription;
	private Button btnCheckOnline;
	private Text txtLatitude;
	private Text txtLongitude;
	private Label lblLatitude;
	private Label lblLongitutde;

	public CreateRoomDialog(SystemController system) {
		this.system = system;
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 * 
	 * @wbp.parser.entryPoint
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(300, 400);
		shell.setText("Create Room");

		Label label = new Label(shell, SWT.NONE);
		label.setText("Name");
		label.setBounds(10, 13, 40, 15);

		txtRoomName = new Text(shell, SWT.BORDER);
		txtRoomName.setBounds(56, 10, 218, 21);

		btnCheckOnline = new Button(shell, SWT.CHECK);
		btnCheckOnline.setText("Online");
		btnCheckOnline.setBounds(10, 304, 264, 16);

		Button btnCreateRoom = new Button(shell, SWT.NONE);
		btnCreateRoom.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				/**
				 * Click Create Button
				 */
				try {
					String room_name = txtRoomName.getText().toString().trim();
					String thumbnail = txtThumbnail.getText().toString().trim();
					String description = txtDescription.getText().toString().trim();
					String latitude = txtLatitude.getText().toString().trim();
					String longitude = txtLongitude.getText().toString().trim();
					double dLatitude, dLongitude;
					boolean status = btnCheckOnline.getSelection();

					/**
					 * Verify inputs
					 */
					if (room_name.length() == 0 || thumbnail.length() == 0 || description.length() == 0 || latitude.length() == 0 || longitude.length() == 0) {
						return;
					}
					try {
						// NullPointerException is checked in .length()
						dLatitude = Double.parseDouble(latitude);
						dLongitude = Double.parseDouble(longitude);
					} catch (NumberFormatException e) {
						return;
					}

					/**
					 * Do action = create_room
					 */
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("action", "create_room");
					jsonObject.put("room_name", room_name);
					jsonObject.put("status", status);
					jsonObject.put("thumbnail", thumbnail);
					jsonObject.put("description", description);
					jsonObject.put("latitude", dLatitude);
					jsonObject.put("longitude", dLongitude);

					System.out.println("req: " + jsonObject);
					String response = ConnectionHandler.doPostString(jsonObject.toString());
					System.out.println("res: " + response);
					if (new JSONObject(response).getBoolean("response") == true) {
						system.lblStatus.setText("Create room success");
					} else {
						system.lblStatus.setText("Create room failed");
					}

				} catch (Exception e) {
					system.lblStatus.setText("Error: Create room failed");
					e.printStackTrace();
				}
				system.refreshRoomList();
				shell.close();
			}
		});
		btnCreateRoom.setText("Create");
		btnCreateRoom.setBounds(117, 326, 75, 25);

		Button btnCancel = new Button(shell, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				shell.close();
			}
		});
		btnCancel.setText("Cancel");
		btnCancel.setBounds(199, 326, 75, 25);

		txtThumbnail = new Text(shell, SWT.BORDER);
		txtThumbnail.setBounds(80, 37, 194, 21);

		Label lblThumbnailUrl = new Label(shell, SWT.NONE);
		lblThumbnailUrl.setText("Thumbnail");
		lblThumbnailUrl.setBounds(10, 43, 64, 15);

		Label lblDescription = new Label(shell, SWT.NONE);
		lblDescription.setBounds(10, 71, 264, 15);
		lblDescription.setText("Description");

		txtDescription = new Text(shell, SWT.BORDER);
		txtDescription.setBounds(10, 92, 264, 146);

		lblLatitude = new Label(shell, SWT.NONE);
		lblLatitude.setBounds(10, 250, 48, 15);
		lblLatitude.setText("Latitude");

		txtLatitude = new Text(shell, SWT.BORDER);
		txtLatitude.setBounds(80, 244, 194, 21);

		txtLongitude = new Text(shell, SWT.BORDER);
		txtLongitude.setBounds(80, 271, 194, 21);

		lblLongitutde = new Label(shell, SWT.NONE);
		lblLongitutde.setText("Longitude");
		lblLongitutde.setBounds(10, 277, 54, 15);

	}
}
