package com.saranomy.sdlscanner.frontdesk;

/**
 * Author: Saran Siripuekpong 5310546561
 * 
 * Updated: 5/26/2014
 */

import java.security.SecureRandom;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class ConnectionHandler {
	public static final int timeout = 2000;
	public static final String url = "https://www.theometas.com:2407/sdl";

	// public static String executePost(String urlParameters) {
	// URL url;
	// HttpURLConnection connection = null;
	// try {
	// // Create connection
	// url = new URL(serverName);
	// connection = (HttpURLConnection) url.FOsopenConnection();
	// connection.setRequestMethod("POST");
	// connection.setRequestProperty("Content-Type", "application/json");
	// connection.setRequestProperty("Content-Length", "" +
	// Integer.toString(urlParameters.getBytes().length));
	// connection.setRequestProperty("Content-Language", "en-US");
	// connection.setConnectTimeout(1000);
	// connection.setUseCaches(false);
	// connection.setDoInput(true);
	// connection.setDoOutput(true);
	//
	// // Send request
	// DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
	// wr.writeBytes(urlParameters);
	// wr.flush();
	// wr.close();
	//
	// // Get Response
	// InputStream is = connection.getInputStream();
	// BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	// String line;
	// StringBuffer response = new StringBuffer();
	// while ((line = rd.readLine()) != null) {
	// response.append(line);
	// response.append('\r');
	// }
	// rd.close();
	// return response.toString();
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// return null;
	//
	// } finally {
	//
	// if (connection != null) {
	// connection.disconnect();
	// }
	// }
	// }

	public static String doPostString(String body) {
		try {
			HttpClient httpClient = createHttpsClient();
			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("Content-Type", "application/json");
			httpPost.setEntity(new StringEntity(body));
			HttpResponse response = httpClient.execute(httpPost);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			return responseHandler.handleResponse(response);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static HttpClient createHttpsClient() {
		try {
			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(null, new TrustManager[] { new CustomX509TrustManager() }, new SecureRandom());

			// set timeout param
			HttpParams params = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(params, timeout);
			HttpConnectionParams.setSoTimeout(params, timeout);
			HttpClient client = new DefaultHttpClient(params);

			SSLSocketFactory ssf = new CustomSSLSocketFactory(ctx);
			ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			ClientConnectionManager ccm = client.getConnectionManager();
			SchemeRegistry sr = ccm.getSchemeRegistry();
			sr.register(new Scheme("https", ssf, 2407));
			DefaultHttpClient sslClient = new DefaultHttpClient(ccm, client.getParams());
			return sslClient;
		} catch (Exception e) {
			return new DefaultHttpClient();
		}
	}

}