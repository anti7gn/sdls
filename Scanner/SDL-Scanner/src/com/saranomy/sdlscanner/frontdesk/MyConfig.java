package com.saranomy.sdlscanner.frontdesk;

/**
 * Author: Saran Siripuekpong 5310546561
 * 
 * Updated: 5/26/2014
 */

import java.awt.Color;

public class MyConfig {
	/**
	 * General
	 */
	public static final String SEPERATOR = "__";

	/**
	 * CreateKeyDialog
	 */
	public static final String CREATE_KEY_TITLE = "Create Key";
	public static final String CREATE_KEY_NAME = "Name";
	public static final String CREATE_KEY_STATUS = "Online";
	public static final String CREATE_KEY_CREATE = "Create";
	public static final String CREATE_KEY_CANCEL = "Cancel";
	public static final String CREATE_KEY_TIME_TYPE = "Time Type";
	public static final String CREATE_KEY_TIME_START = "Time Start";
	public static final String CREATE_KEY_TIME_END = "Time End";

	/**
	 * Frondesk GUI
	 */
	public static final String GUI_TITLE = "Secure Door Lock System";
	public static final String GUI_ROOM_ID = "Room ID";
	public static final String GUI_ROOM_NAME = "Room Name";
	public static final String GUI_THUMBNAIL = "Thumbnail";
	public static final String GUI_DESCRIPTION = "Description";
	public static final String GUI_LATITUDE = "Latitude";
	public static final String GUI_LONGITUDE = "Longitude";
	public static final String GUI_STATUS = "Status";
	public static final String GUI_KEY_AMOUNT = "Key";

	public static final String GUI_KEY_ID = "Key ID";
	public static final String GUI_KEY_NAME = "Key Name";
	public static final String GUI_TIME_MODE = "Time Mode";
	public static final String GUI_TIME_START = "Time Start";
	public static final String GUI_TIME_END = "Time End";

	public static final String GUI_CREATE_ROOM = "Create Room";
	public static final String GUI_CREATE_KEY = "Create Key";
	public static final String GUI_TOGGLE_STATUS = "Toggle Status";
	public static final String GUI_DELETE = "Delete";
	public static final String GUI_BEAM_KEY = "Beam Key";

	/**
	 * Door Scanner GUI
	 */
	public static final String DOOR_GUI_TITLE = "Door Status";
	public static final String DOOR_GUI_ACTIVITY_MIME = "application/com.saranomy.sdl.KeyActivity";
	public static final String DOOR_GUI_STATUS_READY = "Ready to Read";
	public static final String DOOR_GUI_STATUS_CHECK = "Checking";
	public static final String DOOR_GUI_STATUS_PASSED = "Passed";
	public static final String DOOR_GUI_STATUS_FAILED = "Failed";
	public static final String DOOR_GUI_STATUS_ERROR = "Error";
	public static final String DOOR_GUI_STATUS_UNKNOWN = "Unknown App";
	public static final String DOOR_GUI_STATUS_OFFLINE = "Scaner Error";
	public static final Color DOOR_GUI_STATUS_COLOR_RED = new Color(0xFFFF7F7F);
	public static final Color DOOR_GUI_STATUS_COLOR_GREEN = new Color(0xFFA5FF7F);
	public static final Color DOOR_GUI_STATUS_COLOR_GREY = new Color(0xFFEEEEEE);

}
