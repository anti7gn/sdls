package com.saranomy.sdlscanner.door;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.json.JSONArray;
import org.json.JSONObject;
import org.nfctools.utils.LoggingNdefListener;
import org.nfctools.utils.LoggingStatusListener;

import com.saranomy.sdlscanner.frontdesk.ConnectionHandler;
import com.saranomy.sdlscanner.frontdesk.MyConfig;
import com.saranomy.sdlscanner.frontdesk.MyLog;

public class Main {
	public static int room_id = -1;
	private static Map<String, Integer> roomList;

	public static void main(String[] args) {
		new Main();
	}

	public Main() {
		/**
		 * get room list
		 */
		roomList = new HashMap<String, Integer>();
		String[] roomNames = null;
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("action", "room_list");
			MyLog.d("req: " + jsonObject.toString());
			String response = ConnectionHandler.doPostString(jsonObject.toString());
			MyLog.d("res: " + response);
			JSONArray room_list = new JSONArray(response);
			roomNames = new String[room_list.length()];
			for (int i = 0; i < room_list.length(); i++) {
				JSONObject roomObject = room_list.getJSONObject(i);
				roomList.put(roomObject.getString("room_name"), roomObject.getInt("room_id"));
				roomNames[i] = roomObject.getString("room_name");
			}
		} catch (Exception e) {
			return;
		}
			
		/**
		 * Create JFrame
		 */
		JFrame frame = new JFrame(MyConfig.DOOR_GUI_TITLE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JComboBox list = new JComboBox(roomNames);
		list.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String name = (String) ((JComboBox) e.getSource()).getSelectedItem();
				room_id = roomList.get(name);
			}
		});
		frame.getContentPane().add(list, BorderLayout.NORTH);
		JTextField status = new JTextField();
		status.setEditable(false);
		Font font = new Font("Verdana", Font.BOLD, 18);
		status.setFont(font);
		status.setHorizontalAlignment(JLabel.CENTER);
		frame.getContentPane().add(status, BorderLayout.CENTER);
		frame.pack();
		frame.setSize(400, 200);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
		frame.setVisible(true);

		/**
		 * Assign first room_id
		 */
		if (roomNames.length > 0)
			room_id = roomList.get(roomNames[0]);
		
		/**
		 * Run the Scanner listener
		 */
		try {
			LlcpServiceReader scanner = new LlcpServiceReader(this, new LoggingNdefListener(), new LoggingStatusListener(), status);
			scanner.NdefReceiveServer();
		} catch (Exception e) {
			status.setText(MyConfig.DOOR_GUI_STATUS_OFFLINE);
			status.setBackground(MyConfig.DOOR_GUI_STATUS_COLOR_RED);
		}
	}
}