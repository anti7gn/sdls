package com.saranomy.sdlscanner.door;

/**
 * Author: Saran Siripuekpong 5310546561
 * 
 * Updated: 5/26/2014
 */

import java.util.Collection;
import java.util.Iterator;

import javax.swing.JTextField;

import org.json.JSONObject;
import org.nfctools.llcp.LlcpConnectionManager;
import org.nfctools.llcp.LlcpConnectionManagerFactory;
import org.nfctools.llcp.LlcpConstants;
import org.nfctools.llcp.LlcpOverNfcip;
import org.nfctools.ndef.NdefListener;
import org.nfctools.ndef.Record;
import org.nfctools.ndefpush.NdefPushFinishListener;
import org.nfctools.ndefpush.NdefPushLlcpService;
import org.nfctools.scio.Terminal;
import org.nfctools.scio.TerminalHandler;
import org.nfctools.scio.TerminalStatusListener;
import org.nfctools.snep.SnepClient;
import org.nfctools.snep.SnepConstants;
import org.nfctools.snep.SnepServer;
import org.nfctools.spi.acs.AcsTerminal;
import org.nfctools.spi.scm.SclTerminal;
import org.nfctools.utils.LoggingNdefListener;

import com.saranomy.sdlscanner.frontdesk.ConnectionHandler;
import com.saranomy.sdlscanner.frontdesk.MyConfig;
import com.saranomy.sdlscanner.frontdesk.MyLog;

public class LlcpServiceReader {
	private NdefListener ndefListener;
	private NdefPushLlcpService ndefPushLlcpService;
	private Terminal terminal;
	private boolean initiatorMode = false;

	private Main gui;
	private JTextField status;

	public LlcpServiceReader(Main gui, NdefListener ndefListener, TerminalStatusListener statusListener, JTextField status) {
		this.gui = gui;
		this.status = status;
		this.ndefListener = ndefListener;
		TerminalHandler terminalHandler = new TerminalHandler();
		terminalHandler.addTerminal(new AcsTerminal());
		terminalHandler.addTerminal(new SclTerminal());
		terminal = terminalHandler.getAvailableTerminal();
		terminal.setStatusListener(statusListener);
		terminal.setNdefListener(ndefListener);
		status.setText(terminal.getTerminalName());
	}

	public void addMessages(Collection<Record> ndefRecords, NdefPushFinishListener finishListener) {
		ndefPushLlcpService.addMessages(ndefRecords, finishListener);
	}

	public String getTerminalName() {
		return terminal.getTerminalName();
	}

	/*
	 * set up the scanner to wait for Android beam
	 */
	public void NdefReceiveServer() {
		try {
			status.setText(MyConfig.DOOR_GUI_STATUS_READY);
			final SnepServer snepServer = new SnepServer(new MyLoggingNdefListener());
			final SnepClient snepClient = new SnepClient();
			ndefPushLlcpService = new NdefPushLlcpService(ndefListener);
			LlcpOverNfcip llcpOverNfcip = new LlcpOverNfcip(new LlcpConnectionManagerFactory() {

				@Override
				protected void configureConnectionManager(LlcpConnectionManager connectionManager) {
					connectionManager.registerWellKnownServiceAccessPoint(LlcpConstants.COM_ANDROID_NPP, ndefPushLlcpService);
					connectionManager.registerServiceAccessPoint(SnepConstants.SNEP_SERVICE_ADDRESS, snepServer);
					connectionManager.registerServiceAccessPoint(snepClient);
				}
			});

			terminal.setNfcipConnectionListener(llcpOverNfcip);
			if (initiatorMode)
				terminal.initInitiatorDep();
			else
				terminal.initTargetDep();
		} catch (Exception e) {
			status.setText(MyConfig.DOOR_GUI_STATUS_OFFLINE);
			status.setBackground(MyConfig.DOOR_GUI_STATUS_COLOR_RED);
		}
	}

	/*
	 * send TOTP data from Android to server, checking whether it's the same
	 */
	public boolean access_room(String userToken) {
		try {
			String[] split = userToken.split(MyConfig.SEPERATOR);

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("action", "access_room");
			jsonObject.put("room_id", gui.room_id);
			jsonObject.put("key_id", Integer.parseInt(split[0]));
			jsonObject.put("token", split[1]);
			MyLog.d("req: " + jsonObject.toString());
			String response = ConnectionHandler.doPostString(jsonObject.toString());
			System.out.print("res: " + response);
			return new JSONObject(response).getBoolean("response");
		} catch (Exception e) {
			status.setText(MyConfig.DOOR_GUI_STATUS_ERROR);
			status.setBackground(MyConfig.DOOR_GUI_STATUS_COLOR_RED);
		}
		return false;
	}

	/*
	 * loop of listening from scanner driver
	 */
	public class MyLoggingNdefListener extends LoggingNdefListener {

		@Override
		public void doPut(Collection<Record> requestRecords) {
			MyLog.d("\n\n" + requestRecords + "\n\n");
			Iterator<Record> iterator = requestRecords.iterator();
			while (iterator.hasNext()) {
				Record record = iterator.next();
				try {
					if (record.toString().contains(MyConfig.DOOR_GUI_ACTIVITY_MIME)) {
						String[] values = record.toString().split(" Content: ");
						status.setText(MyConfig.DOOR_GUI_STATUS_CHECK);
						status.setBackground(MyConfig.DOOR_GUI_STATUS_COLOR_GREY);
						boolean responseStatus = access_room(values[1]);
						if (responseStatus) {
							status.setText(MyConfig.DOOR_GUI_STATUS_PASSED);
							status.setBackground(MyConfig.DOOR_GUI_STATUS_COLOR_GREEN);
						} else {
							status.setText(MyConfig.DOOR_GUI_STATUS_FAILED);
							status.setBackground(MyConfig.DOOR_GUI_STATUS_COLOR_RED);
						}
					} else {
						status.setText(MyConfig.DOOR_GUI_STATUS_ERROR);
						status.setBackground(MyConfig.DOOR_GUI_STATUS_COLOR_RED);
					}
				} catch (Exception e) {
					status.setText(MyConfig.DOOR_GUI_STATUS_UNKNOWN);
					status.setBackground(MyConfig.DOOR_GUI_STATUS_COLOR_RED);
				}
			}
			super.doPut(requestRecords);
		}
	}
}
